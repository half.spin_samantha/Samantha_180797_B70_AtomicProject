<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 9/17/2017
 * Time: 7:00 PM
 */

namespace App\Model;
use PDO,PDOException;


class Database
{
    public $dbhandle;

    public function __construct()
    {
        try{
            $this->dbhandle = new PDO("mysql:host=localhost;dbname=atomic_project_b70",'root','');

            $this->dbhandle->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

            echo "Database connection successful<br>";
        }

        catch (PDOException $error){

           echo $error->getMessage();
        }
    }

}